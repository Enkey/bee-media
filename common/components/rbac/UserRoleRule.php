<?php

namespace common\components\rbac;

use Yii;
use yii\rbac\Rule;
use yii\helpers\ArrayHelper;
use common\models\User;

class UserRoleRule extends Rule
{
    public $name = 'userRole';
	
    public function init()
    {
		parent::init();
	}
	
	public function execute($user, $item, $params)
    {
        //Получаем массив пользователя из базы
        $user = ArrayHelper::getValue($params, 'user', User::findOne($user));
        $result = false;
//        echo "<pre>".print_r($user, true)."</pre>";
        if ($user) {
            $role = $user->role; //Значение из поля role базы данных

            switch ($item->name) {
                case "admin":
                    $result = $role == User::ROLE_ADMIN;
                    break;
                case "moderator":
                    $result = $role == User::ROLE_ADMIN || $role == User::ROLE_MODERATOR;
                    break;
                case "premium_user":
                    $result = $role == User::ROLE_ADMIN || $role == User::ROLE_MODERATOR
                        ||  $role == User::ROLE_PREMIUM_USER;
                    break;
                case "user":
                    $result = $role == User::ROLE_ADMIN || $role == User::ROLE_MODERATOR
                        ||  $role == User::ROLE_PREMIUM_USER || $role == User::ROLE_USER;
                    break;
            }

//            if($result) {
//                if($user->status == 0)
//                    $result = false;
//            }
        }
        return $result;
    }
}