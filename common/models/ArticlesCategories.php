<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "articles_categories".
 *
 * @property integer $category_id
 * @property integer $article_id
 */
class ArticlesCategories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'articles_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'article_id'], 'required'],
            [['category_id', 'article_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'category_id' => 'Category ID',
            'article_id' => 'Article ID',
        ];
    }
}
