<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "users_categories".
 *
 * @property integer $category_id
 * @property integer $user_id
 */
class UsersCategories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'user_id'], 'required'],
            [['category_id', 'user_id'], 'integer'],
            [['category_id', 'user_id'], 'unique', 'targetAttribute' => ['category_id', 'user_id']]

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'category_id' => 'Category ID',
            'user_id' => 'User ID',
        ];
    }

    public function getCategory() {
        return $this->hasOne(Categories::className(), ['id' => 'category_id']);
    }

    public static function getCategories($userID) {
        $categories = self::find()
                        ->where(['user_id' => $userID])
                        ->with('category')
                        ->asArray()
                        ->all();

        return $categories;
    }
}
