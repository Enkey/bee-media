<?php
namespace common\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;

class LanguageSelector extends Widget
{
    private static $_labels;

    private $_isError;
    private $items;

    public function init()
    {
        $route = Yii::$app->controller->route;
        $appLanguage = Yii::$app->language;
        $params = $_GET;
        $this->_isError = $route === Yii::$app->errorHandler->errorAction;

        array_unshift($params, '/'.$route);

        foreach (Yii::$app->urlManager->languages as $language) {
            $isWildcard = substr($language, -2)==='-*';
            $current = false;
            if (
                $language===$appLanguage ||
                // Also check for wildcard language
                $isWildcard && substr($appLanguage,0,2)===substr($language,0,2)
            ) {
                $current = true;   // Exclude the current language
            }
            if ($isWildcard) {
                $language = substr($language,0,2);
            }
            $params['language'] = $language;
            $this->items[] = [
                'label' => self::label($language),
                'url' => $params,
                'current' => $current
            ];
        }
        parent::init();
    }

    public function run()
    {
        // Only show this widget if we're not on the error page
        if ($this->_isError) {
            return '';
        } else {
            echo Html::beginTag('ul');
            foreach ($this->items as $item) {
                $class = ($item['current']) ? 'text-danger' : '';
                echo Html::beginTag('li');
                echo Html::a($item['label'], $item['url'], ['class' => $class]);
                echo Html::endTag('li');
            }
            echo Html::endTag('ul');

            return parent::run();
        }
    }

    public static function label($code)
    {
        if (self::$_labels===null) {
            self::$_labels = [
                'ru-RU' => 'RU',
                'ro-RO' => 'RO'
            ];
        }

        return isset(self::$_labels[$code]) ? self::$_labels[$code] : $code;
    }
}