<?php
namespace console\controllers;
use Yii;
use yii\console\Controller;
use common\components\rbac\UserRoleRule;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll(); //удаляем старые данные


        //Включаем наш обработчик
        $rule = new UserRoleRule();
        $auth->add($rule);

        //Добавляем роли

        //Пользователь
        $user = $auth->createRole('user');
        $user->description = 'Пользователь';
        $user->ruleName = $rule->name;
        $auth->add($user);

        //Пользователь c прем аккаунтом
        $premiumUser = $auth->createRole('premium_user');
        $premiumUser->description = 'Премиум пользователь';
        $premiumUser->ruleName = $rule->name;
        $auth->add($premiumUser);

        //Модератор
        $moder = $auth->createRole('moderator');
        $moder->description = 'Модератор';
        $moder->ruleName = $rule->name;
        $auth->add($moder);

        //Добавляем потомков
        $auth->addChild($premiumUser, $user);
        $auth->addChild($moder, $premiumUser);
//        $auth->addChild($moder, $deleteUser);

        //Администратор
        $admin = $auth->createRole('admin');
        $admin->description = 'Администратор';
        $admin->ruleName = $rule->name;
        $auth->add($admin);

        $auth->addChild($admin, $moder);
    }
}