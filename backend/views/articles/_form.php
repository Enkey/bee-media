<?php

use common\models\Tags;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Articles */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="articles-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'short_description')->textarea(['maxlength' => 255]) ?>
    <?= $form->field($model, 'comment')->textarea(['maxlength' => 255]) ?>


    <?= \yii\redactor\widgets\Redactor::widget([
        'model' => $model,
        'attribute' => 'text'
    ]) ?>

    <?php
    echo $form->field($model, 'tags_valid')->widget(Select2::classname(), [
        'data' => $tags,
        'options' => ['placeholder' => 'Selectati taguri corecte', 'multiple' => true],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);

    ?>
    <?php
    echo $form->field($model, 'tags_invalid')->widget(Select2::classname(), [
        'data' => $tags,
        'options' => ['placeholder' => 'Selectati taguri incorecte', 'multiple' => true],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);

    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
