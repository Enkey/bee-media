<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;
use yii\widgets\Menu;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?> | Admin BeeMedia</title>
    <link href="https://fonts.googleapis.com/css?family=Raleway:500" rel="stylesheet">
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <section class="main-wrapper">
        <aside class="fixed">
            <div class="top-logo">
                <a href="<?= Url::to(['/site/index']) ?>">
                    <img src="<?= Url::to(['/img/logo_big_first_blog.png'], true) ?>" alt="BeeMedia">
                </a>
            </div>
            <?php
            echo Menu::widget([
                'items' => [
                    // Important: you need to specify url as 'controller/action',
                    // not just as 'controller' even if default action is used.
                    ['label' => '<span><i class="fa fa-tasks"></i></span>Sarcine', 'url' => ['articles/index']],
                    ['label' => '<span><i class="fa fa-book"></i></span>Categorii', 'url' => ['categories/index']],
                    ['label' => '<span><i class="fa fa-line-chart"></i></span>Taguri', 'url' => ['tags/index']]
                ],
                'options' => [
                    'class' => 'aside-nav'
                ],
                'encodeLabels' => false,
            ]);
            ?>
        </aside>
        <div class="main-content container-fluid">
            <?= $content ?>
        </div>
    </section>


</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

