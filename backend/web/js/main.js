$(function () {
    $('#tags_list li a').on('click', function () {
        var parentLi = $(this).closest('li');
        if(parentLi.hasClass('active')) {
            $(this).find('input').prop('disabled', true);
            parentLi.removeClass('active');
        } else {
            $(this).find('input').prop('disabled', false);
            parentLi.addClass('active');
        }
        return false;
    });
});