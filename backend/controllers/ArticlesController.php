<?php

namespace backend\controllers;

use common\models\ArticlesTags;
use common\models\Tags;
use Yii;
use common\models\Articles;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ArticlesController implements the CRUD actions for Articles model.
 */
class ArticlesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Articles models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Articles::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Articles model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Articles model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Articles();

        if ($model->load(Yii::$app->request->post())) {
            $model->user_id = Yii::$app->user->id;
            if($model->save()) {
                foreach ($model->tags_valid as $validTag) {
                    $articleTag = new ArticlesTags();
                    $articleTag->article_id = $model->id;
                    $articleTag->tag_id = $validTag;
                    $articleTag->correct = 1;
                    $articleTag->save();
                }
                foreach ($model->tags_invalid as $invalidTag) {
                    $articleTag = new ArticlesTags();
                    $articleTag->article_id = $model->id;
                    $articleTag->tag_id = $invalidTag;
                    $articleTag->correct = 0;
                    $articleTag->save();
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'tags' => $this->getAllTags()
            ]);
        }
    }

    /**
     * Updates an existing Articles model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);


        if ($model->load(Yii::$app->request->post())) {

            ArticlesTags::deleteAll(['article_id' => $id]);
            foreach ($model->tags_valid as $validTag) {
                $articleTag = new ArticlesTags();
                $articleTag->article_id = $id;
                $articleTag->tag_id = $validTag;
                $articleTag->correct = 1;
                $articleTag->save();
            }
            foreach ($model->tags_invalid as $invalidTag) {
                $articleTag = new ArticlesTags();
                $articleTag->article_id = $id;
                $articleTag->tag_id = $invalidTag;
                $articleTag->correct = 0;
                $articleTag->save();
            }
            if($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $articleTags = $this->getArticleTags($id);
            $validIDs = [];
            $invalidIDs = [];
            foreach ($articleTags['valid'] as $id => $articleTag) {
                $validIDs[] = $id;
            }
            foreach ($articleTags['invalid'] as $id => $articleTag) {
                $invalidIDs[] = $id;
            }
            $model->tags_valid = $validIDs;
            $model->tags_invalid = $invalidIDs;
            return $this->render('update', [
                'model' => $model,
                'tags' => $this->getAllTags(),
                ''
            ]);
        }
    }

    public function getArticleTags($articleID) {
        $articlesTags = ArticlesTags::find()->where(['article_id' => $articleID])->with('tag')->asArray()->all();
        $articlesTagsArray = [
            'valid' => [],
            'invalid' => [],
        ];

        if(!empty($articlesTags)) {
            foreach ($articlesTags as $articlesTag) {
                $index = ($articlesTag['correct'] == 1) ? 'valid' : 'invalid';
                $articlesTagsArray[$index][$articlesTag['tag']['id']] = $articlesTag['tag']['name'];
            }
        }

        return $articlesTagsArray;

    }

    public function getAllTags() {
        $tags = Tags::find()->asArray()->all();
        $tagsArray = [];
        foreach ($tags as $tag) {
            $tagsArray[$tag['id']] = $tag['name'];
        }
        return $tagsArray;
    }

    /**
     * Deletes an existing Articles model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Articles model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Articles the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Articles::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
