<?php

/** @var $article \common\models\Articles */

$this->title = $article->title;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html; ?>

<div class="row">
    <div class="col-xs-9">
        <h2 class="page-title"><?= $article->title ?></h2>
        <div class="lesson-body">
            <?= $article->text ?>
        </div>
    </div>
    <div class="col-xs-3">
        <?php if(!empty($tags)):?>
            <?php $form = ActiveForm::begin(); ?>
            <h2 class="page-title">Analiza resursei:</h2>
            <div class="filter-block form-group">
                <div class="filter-content">
                    <ul class="nav nav-pills nav-stacked checkbox-list" id="tags_list">
                        <?php foreach ($tags as $tag): ?>
                            <li>
                                <a href="#">
                                    <span><i class="fa fa-check"></i></span> <?= $tag['name'] ?>
                                    <input type="hidden" disabled name="LessonsResults[result_tags_array][]"
                                           value="<?= $tag['id'] ?>">
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
            <div class="form-group">
                <?= Html::submitButton('Submit', ['class' => 'btn btn-success btn-block']) ?>
            </div>
            <?php ActiveForm::end(); ?>

            <button class="btn btn-primary" type="button"
                    data-toggle="collapse" data-target="#adminComment" aria-expanded="false" aria-controls="adminComment">
                <i class="fa fa-comment"></i> Comentariu
            </button>
            <div class="collapse" id="adminComment">
                <br>
                <div class="well">
                    <?=$article->comment?>
                </div>
            </div>
        <?php endif;?>
<!--        <div class="filter-block">-->
<!--            <div class="filter-title">Грамматика</div>-->
<!--            <div class="filter-content">-->
<!--                <ul class="nav nav-pills nav-stacked checkbox-list checkbox-list-one">-->
<!--                    <li><a href="#"><span><i class="fa fa-check"></i></span> Пунктуация</a></li>-->
<!--                    <li><a href="#"><span><i class="fa fa-check"></i></span> Диакритики</a></li>-->
<!--                    <li><a href="#"><span><i class="fa fa-check"></i></span> Лингвистические</a></li>-->
<!--                </ul>-->
<!--            </div>-->
<!--        </div>-->

    </div>
</div>


