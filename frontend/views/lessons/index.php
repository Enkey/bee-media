<?php

/** @var $articles \common\models\Articles */
use yii\helpers\Url; ?>
<?php

/* @var $this yii\web\View */

$this->title = 'Sarcine';
?>

<h2 class="page-title">Sarcine</h2>
<div class="articles-list">
    <?php foreach ($articles as $article): ?>
        <article>
            <div class="article-image">
                <img src="http://lorempixel.com/100/100/business/<?=$article->id?>" alt="">
            </div>
            <div class="article-content">
                <div class="article-title"><?= $article->title ?></div>
                <div class="article-description">
                    <?= $article->short_description ?>
                </div>
                <div class="article-buttons">
                    <a href="<?= Url::to(['lessons/view', 'id' => $article->id]) ?>" class="btn btn-info">Alege articol</a>
                </div>
            </div>
        </article>
    <?php endforeach; ?>
</div>
<!---->
<!--<div class="col-xs-3">-->
<!--    <h2 class="page-title">Фильтр заданий</h2>-->
<!--    <div class="filter-block">-->
<!--        <div class="filter-title">Тип задания</div>-->
<!--        <div class="filter-content">-->
<!--            <ul class="nav nav-pills nav-stacked checkbox-list">-->
<!--                <li class="checked"><a href="#"><span><i class="fa fa-check"></i></span> Оценивании статьи</a></li>-->
<!--                <li><a href="#"><span><i class="fa fa-check"></i></span> Правда/ложь</a></li>-->
<!--                <li><a href="#"><span><i class="fa fa-check"></i></span> Пропаганда</a></li>-->
<!--            </ul>-->
<!--        </div>-->
<!--    </div>-->
<!--    <div class="filter-block">-->
<!--        <div class="filter-title">Сложность</div>-->
<!--        <div class="filter-content">-->
<!--            <ul class="nav nav-pills nav-stacked checkbox-list checkbox-list-one">-->
<!--                <li><a href="#"><span><i class="fa fa-dot-circle-o"></i></span> Легкая</a></li>-->
<!--                <li class="checked"><a href="#"><span><i class="fa fa-dot-circle-o"></i></span> Средняя</a></li>-->
<!--                <li><a href="#"><span><i class="fa fa-dot-circle-o"></i></span> Сложная</a></li>-->
<!--            </ul>-->
<!--        </div>-->
<!--    </div>-->
<!---->
<!--</div>-->

