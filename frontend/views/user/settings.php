<?php
/** @var $categories Categories */
/** @var $user \common\models\User */
use common\models\Categories;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;

$this->title = 'User`s settings';
?>
<h1>User Settings</h1>

<?php
$form = ActiveForm::begin([
    'id' => 'form-input-example',
    'options' => [
        'class' => 'form-horizontal col-lg-11'
    ],
]);
?>
<?php
echo $form->field($user, 'categories_ids')->widget(Select2::classname(), [
    'data' => ArrayHelper::map(Categories::find()->all(), 'id', 'title'),
    'options' => ['placeholder' => 'Selectati taguri incorecte', 'multiple' => true],
    'pluginOptions' => [
        'allowClear' => true
    ],
]);

?>

<div class="form-group">
    <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
</div>
<?php
ActiveForm::end();
?>
