<?php

/* @var $this yii\web\View */

use common\models\User;
use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
    <h1><?=Yii::t('app','Тест')?></h1>

    <?= \common\widgets\LanguageSelector::widget();?>

    <h2>Current language is <?=Yii::$app->language?></h2>
    <?= \yii\redactor\widgets\Redactor::widget([
        'model' => User::findIdentity(1),
        'attribute' => 'body'
    ]) ?>
    <code><?= __FILE__ ?></code>
</div>
