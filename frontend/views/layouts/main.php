<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\widgets\Menu;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?> | BeeMedia</title>
    <link href="https://fonts.googleapis.com/css?family=Raleway:500" rel="stylesheet">
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <section class="main-wrapper">
        <aside class="fixed">
            <div class="top-logo">
                <a href="<?= Url::to(['/site/index']) ?>">
                    <img src="<?= Url::to(['/img/logo_big_first_blog.png'], true) ?>" alt="BeeMedia">
                </a>
            </div>
            <a href="<?= Url::to(['/user/profile']) ?>" class="user-widget">
                <div class="user-avatar">
                    <img src="https://placehold.it/60" alt="">
                </div>
                <div class="user-name">User Name</div>
            </a>
            <?php
            echo Menu::widget([
                'items' => [
                    // Important: you need to specify url as 'controller/action',
                    // not just as 'controller' even if default action is used.
                    ['label' => 'Sarcine', 'url' => ['lessons/index'], 'icon' => 'fa-tasks', 'options' => ['class' => 'lessons-icon']],
                    ['label' => 'Biblioteca', 'url' => ['library/index'], 'options' => ['class' => 'library-icon']],
                    ['label' => 'Progresul', 'url' => ['user/progress'], 'options' => ['class' => 'progress-icon']]
                ],
                'options' => [
                    'class' => 'aside-nav'
                ],
                'encodeLabels' => false,
            ]);
            ?>
        </aside>
        <div class="main-content container-fluid">
            <?= $content ?>
        </div>
    </section>


</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
