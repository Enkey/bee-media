<?php
namespace frontend\controllers;

use common\models\Articles;
use common\models\ArticlesTags;
use common\models\Categories;
use common\models\LessonsResults;
use common\models\Tags;
use common\models\User;
use common\models\UsersCategories;
use Yii;
use yii\web\Controller;

/**
 * Articles controller
 */
class LessonsController extends Controller
{

    public function actionIndex()
    {
        $articles = Articles::find()->orderBy('id DESC')->all();
        return $this->render('index', [
            'articles' => $articles
        ]);
    }

    public function actionView($id)
    {
        $article = Articles::findOne(['id' => $id]);
        $lessonResult = new LessonsResults();

        if ($lessonResult->load(Yii::$app->request->post())) {
            if (!empty($lessonResult->result_tags_array)) {
                $lessonResult->result_tags = implode(',', $lessonResult->result_tags_array);
                $lessonResult->article_id = $id;
                $lessonResult->user_id = Yii::$app->user->id;

                if ($lessonResult->save()) {
                    return $this->redirect(['lessons/result', 'id' => $lessonResult->id]);
                }
            }
        }

        return $this->render('view', [
            'tags' => $this->getArticleTags($id),
            'article' => $article
        ]);
    }
    /*
     * invalid -1
     * valid +2
     */

    public function actionResult($id)
    {
        $lessonResult = LessonsResults::findOne(['id' => $id, 'user_id' => Yii::$app->user->id]);
        if(!empty($lessonResult)) {
            $articleTags = ArticlesTags::find()
                ->where(['article_id' => $lessonResult->article_id])
                ->with('tag')
                ->asArray()
                ->all();

            $article = Articles::find()->where(['id' => $lessonResult->article_id])->one();

            $resultTagsIDs = explode(',',$lessonResult->result_tags);
            $usersTags = Tags::find()->where(['id' => $resultTagsIDs])->all();
            $validTags = [];
            $validTagIDs = [];
            $percent = 0;

            if(!empty($resultTagsIDs)) {
                foreach ($articleTags as $articleTag) {
                    if($articleTag['correct']) {
                        $validTagIDs[] = $articleTag['tag_id'];
                        $validTags[] = $articleTag['tag']['name'];
                    }
                }

                $points = 0;
                $totalPoints = count($validTagIDs) * 2;
                foreach ($resultTagsIDs as $resultTagID) {
                    if(in_array($resultTagID, $validTagIDs)) {
                        $points += 2;
                    } else {
                        $points -= 1;
                    }
                }

                $percent = ($points * 100) / $totalPoints;

            }

            return $this->render('result', [
                'userTags' => $usersTags,
                'validTags' => $validTags,
                'lessonResult' => $lessonResult,
                'percent' => $percent,
                'article' => $article
            ]);
        }
        return $this->redirect(['lessons/index']);

    }

    public function getArticleTags($articleID)
    {
        $articlesTags = ArticlesTags::find()->where(['article_id' => $articleID])->with('tag')->asArray()->all();
        $articlesTagsArray = [];

        if (!empty($articlesTags)) {
            foreach ($articlesTags as $articlesTag) {
                $articlesTagsArray[] = [
                    'id' => $articlesTag['tag']['id'],
                    'name' => $articlesTag['tag']['name']
                ];
            }
        }

        shuffle($articlesTagsArray);

        return $articlesTagsArray;

    }

}
