<?php
namespace frontend\controllers;

use common\models\Articles;
use common\models\Categories;
use common\models\User;
use common\models\UsersCategories;
use Yii;
use yii\web\Controller;

/**
 * Library controller
 */
class LibraryController extends Controller
{

    public function actionIndex()
    {
        $articles = Articles::find()->all();
        return $this->render('index',[
            'articles' => $articles
        ]);
    }

//    public function actionView($id)
//    {
//        $article = Articles::findOne(['id' => $id]);
//        return $this->render('view', [
//            'article' => $article
//        ]);
//    }

}
