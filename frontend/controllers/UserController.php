<?php
namespace frontend\controllers;

use common\models\Categories;
use common\models\User;
use common\models\UsersCategories;
use Yii;
use yii\web\Controller;

/**
 * User controller
 */
class UserController extends Controller
{

    public function actionProfile()
    {
        return $this->render('profile');
    }
    public function actionProgress()
    {
        return $this->render('progress');
    }

    public function actionSettings()
    {
        $user = new User();
        $userCategoriesData = UsersCategories::getCategories(Yii::$app->user->id);

        $categoriesArray = [];
        if(!empty($userCategoriesData)) {
            foreach ($userCategoriesData as $item) {
                $categoriesArray[] = $item['category']['id'];
            }
        }

        if ($user->load(Yii::$app->request->post())) {
            if(!empty($user->categories_ids)) {
                $userID = Yii::$app->user->id;
                $categoriesArray = [];
                UsersCategories::deleteAll('user_id = :userID',['userID' => $userID]);
                foreach ($user->categories_ids as $category_id) {
                    $articleTag = new UsersCategories();
                    $articleTag->category_id = $category_id;
                    $articleTag->user_id = $userID;
                    $articleTag->save();

                    $categoriesArray[] = $category_id;
                }
            }
        }

        $user->categories_ids = $categoriesArray;
        return $this->render('settings', [
            'user' => $user
        ]);
    }
    public function actionSettingsSave()
    {
        $userCategories = UsersCategories::getCategories(Yii::$app->user->id);
        return $this->render('settings');
    }

}
